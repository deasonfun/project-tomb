import pygame
import random

#Game variables
running = False

w_width, w_height = 800, 800
background_color = (0,100,100)

#player variables
c_color = (255, 0, 255)
c_x, c_y = 0, 0
c_width, c_height = 200, 200

player_health = 200

player_turn = True

#Enemy variables
e_color = (255, 0, 0)
enemy_health = 50
#Randomize position from 200 to 600 by 200
e_x, e_y = random.randrange (200, 600, 200), random.randrange (200, 600, 200)

#Set window params
(width, height) = (w_width, w_height)
window = pygame.display.set_mode ((width, height))
window_rect=window.get_rect()
pygame.display.set_caption ('project-tomb')

running = True

while running:
    #Draw window
    window.fill (background_color)
    #Define and draw player
    player = pygame.Rect(c_x, c_y, c_width, c_height)
    pygame.draw.rect(window, c_color, player)
    #Define and draw enemy
    enemy = pygame.Rect(e_x, e_y, c_width, c_height)
    pygame.draw.rect(window, e_color, enemy)

    if player_health <= 0:
        print ("Game over")
        running = False

    if enemy_health <= 0:
        print ("You win!")
        running = False

    pygame.display.update()

    for event in pygame.event.get():
        if player_turn:
            #Player movement
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    c_x -= 200
                    player_turn = False
                if event.key == pygame.K_RIGHT:
                    c_x += 200
                    player_turn = False
                if event.key == pygame.K_UP:
                    c_y -= 200
                    player_turn = False
                if event.key == pygame.K_DOWN:
                    c_y += 200
                    player_turn = False
                #Player attacks
                if event.key == pygame.K_SPACE:
                    if c_x == e_x + 200 and c_y == e_y:
                        enemy_health -= 10
                    if c_x == e_x - 200 and c_y == e_y:
                        enemy_health -= 10
                    if c_y == e_y + 200 and c_x == e_x:
                        enemy_health -= 10
                    if c_y == e_y - 200 and c_x == e_x:
                        enemy_health -= 10
                player_turn = False

            #Stops player from going outside window
            if c_x < 0:
                c_x = 0
            if c_y < 0:
                c_y = 0
            if c_x > 600:
                c_x = 600
            if c_y > 600:
                c_y = 600

        #Enemy AI
        if not player_turn:
            print ("Enemy turn")
            print ("Enemy health: ", enemy_health)
            if c_y <= 200:
                e_y -= 200
                player_turn = True
            if c_y > 200:
                e_y += 200
                player_turn = True
            if c_x <= 200:
                e_x -= 200
                player_turn = True
            if c_x > 200:
                e_x += 200
                player_turn = True
        else:
            print ("Player turn")
            print ("Player health: ", player_health)

        #Stops Enemy from overlaping the player
        if c_x == 600 and e_x == 600:
            e_x -= 200
        if c_x <= 0 and e_x <= 0:
            e_x += 200

        #Stops Enemy from going outside window
        if e_x < 0:
            e_x = 0
        if e_y < 0:
            e_y = 0
        if e_x > 600:
            e_x = 600
        if e_y > 600:
            e_y = 600

        #Enemy attacks
        if e_x == c_x - 200 and e_y == c_y:
            player_health -= 10
        if e_x == c_x + 200 and e_y == c_y:
            player_health -= 10
        if e_y == c_y - 200 and e_x == c_x:
            player_health -= 10
        if e_y == c_y + 200 and e_x == c_x:
            player_health -= 10

        #Quits the game
        if event.type == pygame.QUIT:
            running = False
